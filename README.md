<div align="center">
<p align="center">
   <img src=".github/image/ico.jpg" height="350" alt="非常感谢 `别哭草莓菠菜^ ^ 微信号：Xwenlilililili_` 绘制的图像" title="非常感谢 `别哭草莓菠菜^ ^ 微信号：Xwenlilililili_` 绘制的图像">
</p>

# ZeroKingBot

[![Java CI Package](https://github.com/KingPrimes/ZeroKingBot/actions/workflows/release.yml/badge.svg)](https://github.com/KingPrimes/ZeroKingBot/actions/workflows/release.yml)

</div>

----

<div align="center">
<h3>一个基于 Java 编写的 QQ 机器人</h3>
<div>    
    <p>该机器人主要用于 Warframe 游戏的查询</p>
    <p>若您在使用过程中发现了Bug或者有一些建议，欢迎提出<a href="https://github.com/KingPrimes/ZeroKingBot/issues">ISSUE</a>、PR或加入 <a href="https://jq.qq.com/?_wv=1027&k=HkG2Q9PT">QQ交流群：260079469</a></p>
</div>
</div>

项目介绍
---
---
- <p>原名 <a href="https://github.com/KingPrimes/TWGBot">TWGBot</a> 现已更名为 <a href="https://github.com/KingPrimes/ZeroKingBot">ZeroKingBot</a></p>
- 图片渲染
  <br/>
  采用了Html+css的方式渲染图片使用到了 ”**org.xhtmlrenderer**“ 库
  <br/>
  Html文件在 [ZKBotImageHtml](https://github.com/KingPrimes/ZKBotImageHtml) 库中。您如果对默认生成的图片不满意，可随意更改Html/Css文件。
  <br/>


项目部署
---
---
- [Windows部署文档](./readme/win.md)
- [Linux部署文档](./readme/linux.md)


使用文档
---
---
- [文档链接](https://www.yuque.com/kingprimes/twgbot)


服务器资源推荐
---
- [阿里云优惠](https://www.aliyun.com/minisite/goods?userCode=8dt5pt0g&share_source=copy_link)
- [腾讯云优惠](https://cloud.tencent.com/act/pro/cps_3?fromSource=gwzcw.6688284.6688284.6688284&cps_key=ae3b8b6e55495d8bc53f2227ea0273d8)
- [腾讯云免费体验](https://cloud.tencent.com/act/free)

贡献者
---
---
- 非常感谢 `别哭草莓菠菜^ ^ 微信号：Xwenlilililili_` 绘制的图像
<!-- readme: collaborators,contributors -start -->
<!-- readme: collaborators,contributors -end -->


赞助作者
---
---
<img src=".github/image/upA-W.png" width="500"/>


鸣谢
---
---
- Thanks [JetBrains](https://www.jetbrains.com/?from=Shiro) Provide Free License Support OpenSource Project

[<img src="https://mikuac.com/images/jetbrains-variant-3.png" width="200"/>](https://www.jetbrains.com/?from=mirai)

Stargazers over time
---
---
[![Stargazers over time](https://starchart.cc/KingPrimes/ZeroKingBot.svg)](https://starchart.cc/KingPrimes/ZeroKingBot)
